//
//  BasicAppTests.swift
//  BasicAppTests
//
//  Created by herbal7ea on 4/10/19.
//  Copyright © 2019 Jon Bott. All rights reserved.
//

import XCTest
@testable import BasicApp

class NetworkTests: XCTestCase {

    private var classUnderTest = NetworkLayer()
    
    // MARK: - Integration Test
    func test_loadUser_successBlockCalled() {
        //Step 01 - Test for loading data (fails by default)
    }
    
    // MARK: - Unit Test
    func test_loadUser_ParseJSON() {
        //Step 04 - Decode JSON
    }
}




