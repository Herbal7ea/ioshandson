//
//  Extensions.swift
//  iOSHandsOn
//
//  Created by herbal7ea on 4/9/19.
//  Copyright © 2019 Jon Bott. All rights reserved.
//

import UIKit

extension UIStoryboard {
    static var main: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }
}
