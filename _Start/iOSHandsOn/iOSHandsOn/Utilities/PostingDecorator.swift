//
//  PostingDecorator.swift
//  iOSHandsOn
//
//  Created by herbal7ea on 2018/10/16.
//  Copyright © 2018 Jon Bott. All rights reserved.
//

import UIKit

typealias ImageClosure = (UIImage?) -> Void

class PostingDecorator {
    
    let networkLayer: NetworkLayer
    
    init(networkLayer: NetworkLayer) {
        self.networkLayer = networkLayer
    }
    
    func loadImage(for posting: Posting, onImageLoaded: @escaping ImageClosure) -> URLSessionDataTask? {
        guard posting.profileImage == nil,
            let url = URL(string: posting.profileImageUrl)
            else { onImageLoaded(posting.profileImage) ; return nil }
        
        let imageTask = networkLayer.loadImage(url: url) { image in
            posting.profileImage = image
            onImageLoaded(image)
        }
        
        return imageTask
    }
}

