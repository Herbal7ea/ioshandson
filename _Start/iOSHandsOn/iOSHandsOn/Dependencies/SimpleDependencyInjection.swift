//
//  SimpleDependencyInjection
//  iOSHandsOn
//
//  Created by herbal7ea on 4/9/19.
//  Copyright © 2019 Jon Bott. All rights reserved.
//

import UIKit

class SimpleDependencyInjection {
    static var shared = SimpleDependencyInjection()
    
    
    // - MARK: Singletons
    var navigationRouter: NavigationRouter!
    var networkLayer: NetworkLayer = NetworkLayerReal() // NetworkLayerMocked() //  
    var postingDecorator: PostingDecorator {
        return PostingDecorator(networkLayer: networkLayer)
    }
    
    // - MARK: Initializer
    func initWith(_ navigationController: UINavigationController) {
        guard self.navigationRouter == nil else { return }

        self.navigationRouter = NavigationRouter(with: navigationController)
    }
    
    // - MARK: New Instances
    var tableViewPresenter: TableViewPresenter { return TableViewPresenter(networkLayer: networkLayer) }
    
}


// - MARK: Injection Methods
extension SimpleDependencyInjection {
    
    func prepare(_ tableViewController: TableViewController) {
        tableViewController.injectDependencies(navigationRouter: navigationRouter, tableViewPresenter: tableViewPresenter)
    }

}
