//
//  PostingCell.swift
//  iOSHandsOn
//
//  Created by herbal7ea on 2018/10/16.
//  Copyright © 2018 Jon Bott. All rights reserved.
//

import UIKit

class PostingCell: UITableViewCell {
    
    static var cellId = "PostingCell"
    
    @IBOutlet var username: UILabel!
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var descriptionLabel: UILabel!
    
    private var imageTask: URLSessionDataTask?
    private let cellDecorator = SimpleDependencyInjection.shared.postingDecorator
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        imageTask?.cancel()
        
        username.text = ""
        profileImage.image = nil
        descriptionLabel.text = ""
    }

    func configure(with posting: Posting) {
        let id = posting.userId != nil ? "User \(posting.userId!)" : ""
        username.text = id
        
        descriptionLabel.text = posting.title
        
        imageTask = cellDecorator.loadImage(for: posting) { [weak self] image in
            self?.profileImage.image = image
        }
    }
}








