//
//  NavigationRouter.swift
//  iOSHandsOn
//
//  Created by herbal7ea on 2018/10/16.
//  Copyright © 2018 Jon Bott. All rights reserved.
//

import UIKit

class NavigationRouter {
    
    let rootNavigationController: UINavigationController
    
    init(with navigationController: UINavigationController) {
        rootNavigationController = navigationController
    }
}

// - MARK: - Navigation Methods
extension NavigationRouter {
    func postingRowSelected(with posting: Posting) {
        
        let vc = UIStoryboard.main.instantiateViewController(withIdentifier: DetailViewController.viewControllerId) as! DetailViewController
            vc.prepare(with: posting)
        
        rootNavigationController.pushViewController(vc, animated: true)
    }
}









