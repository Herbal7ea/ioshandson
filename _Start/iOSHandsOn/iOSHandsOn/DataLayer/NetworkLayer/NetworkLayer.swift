//
//  NetworkLayer.swift
//  iOSHandsOn
//
//  Created by herbal7ea on 2018/10/16.
//  Copyright © 2018 Jon Bott. All rights reserved.
//

import UIKit

protocol NetworkLayer {
    func loadItems(onPostingsLoaded: @escaping PostingsClosure)
    func loadImage(url: URL, onImageLoaded: @escaping ImageClosure) -> URLSessionDataTask?
}

class NetworkLayerReal: NetworkLayer {

    func loadItems(onPostingsLoaded: @escaping PostingsClosure) {
        let urlString = "https://jsonplaceholder.typicode.com/posts"
        let url = URL(string: urlString)!
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else { onPostingsLoaded([]) ; return }
            
            let postings = try! JSONDecoder().decode([Posting].self, from: data)
            
            DispatchQueue.main.sync {
                onPostingsLoaded(postings)
            }
        }.resume()
    }

    func loadImage(url: URL, onImageLoaded: @escaping ImageClosure) -> URLSessionDataTask? {
        let imageTask = URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else { print(error!) ; return }
            
            let profileImage = UIImage(data: data)
            
            DispatchQueue.main.async {
                onImageLoaded(profileImage)
            }
        }
        
        imageTask.resume()
        
        return imageTask
    }
}


class NetworkLayerMocked: NetworkLayer {

    func loadImage(url: URL, onImageLoaded: @escaping ImageClosure) -> URLSessionDataTask? {
        return nil
    }
    
    func loadItems(onPostingsLoaded: @escaping PostingsClosure) {
        //fake network delay // 1 second
        DispatchQueue.global().asyncAfter(deadline: .now() + 1) {
            let postings = self.buildFakePostings()
            
            DispatchQueue.main.sync {
                onPostingsLoaded(postings)
            }
        }
    }
}

// - MARK: private methods
extension NetworkLayerMocked {
    private func buildFakePostings() -> [Posting] {
        return stride(from: 1, to: 30, by: 1)
                 .map { Posting(userId: $0,
                                    id: $0,
                                 title: "Fake title for \($0!)",
                                  body: "Body for Posting \($0!)")
                      }
    }
}



