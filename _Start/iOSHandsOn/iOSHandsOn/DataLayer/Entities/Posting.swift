//
//  Posting.swift
//  iOSHandsOn
//
//  Created by herbal7ea on 2018/10/16.
//  Copyright © 2018 Jon Bott. All rights reserved.
//

import UIKit

class Posting: Codable {
    var userId: Int?
    var id: Int?
    var title: String?
    var body: String?
    
    var profileImageUrl: String = "https://loremflickr.com/100/100/tsukiji/" //"https://ipsumimage.appspot.com/100x100?f=ff7700" // //"https://source.unsplash.com/random/100x100#\(Date().timeIntervalSince1970)" //http://lorempixel.com/100/100/"
    var profileImage: UIImage?
    
    init(userId: Int?, id: Int?, title: String?, body: String?) {
        self.userId = userId
        self.id = id
        self.title = title
        self.body = body
    }
    
    enum CodingKeys: String, CodingKey {
        case userId,
                 id,
              title,
               body
    }
}





