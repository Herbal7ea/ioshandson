//
//  TableViewPresenter.swift
//  iOSHandsOn
//
//  Created by herbal7ea on 2018/10/16.
//  Copyright © 2018 Jon Bott. All rights reserved.
//

import Foundation

typealias VoidClosure = () -> Void
typealias PostingsClosure = ([Posting]) -> Void


class TableViewPresenter {
    var postings = [Posting]()
    
    private var networkLayer: NetworkLayer
    
    init(networkLayer: NetworkLayer) {
        self.networkLayer = networkLayer
    }
    
    
    func preloadItems(onItemsLoaded: @escaping VoidClosure) {
        networkLayer.loadItems { [weak self] postings in
            self?.postings = postings
            onItemsLoaded()
        }
    }
}








