//
//  ViewController.swift
//  iOSHandsOn
//
//  Created by herbal7ea on 2018/10/12.
//  Copyright © 2018 Jon Bott. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {

    var presenter: TableViewPresenter!
    var navigationRouter: NavigationRouter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SimpleDependencyInjection.shared.initWith(navigationController!)
        SimpleDependencyInjection.shared.prepare(self)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int { return 1 }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return presenter.postings.count }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let posting = presenter.postings[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: PostingCell.cellId, for: indexPath) as! PostingCell
            cell.configure(with: posting)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let posting = presenter.postings[indexPath.row]
        
        navigationRouter.postingRowSelected(with: posting)
    }
    
    func injectDependencies(navigationRouter: NavigationRouter, tableViewPresenter: TableViewPresenter) {
        self.navigationRouter = navigationRouter
        self.presenter = tableViewPresenter
        
        presenter.preloadItems {
            self.tableView.reloadData()
        }
    }
}





