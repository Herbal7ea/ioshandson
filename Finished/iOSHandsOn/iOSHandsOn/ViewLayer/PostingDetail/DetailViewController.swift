//
//  DetailViewController.swift
//  iOSHandsOn
//
//  Created by herbal7ea on 2018/10/15.
//  Copyright © 2018 Jon Bott. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    static var viewControllerId = "DetailViewController"
    
    @IBOutlet var userIdLabel: UILabel!
    @IBOutlet var messageIdLabel: UILabel!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var bodyTextView: UITextView!
    @IBOutlet var profileImage: UIImageView!
    
    private var posting: Posting!
    
    @IBAction func commentTapped(_ sender: Any) {
        print("🦄 comment button tapped.  Do something")
    }
    
    func prepare(with posting: Posting) {
        self.posting = posting
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let userId = posting.userId != nil ? "User \(posting.userId!)" : ""
        let id = posting.id != nil ? "\(posting.id!)" : ""
        
        userIdLabel.text = userId
        messageIdLabel.text = id
        titleLabel.text = posting.title
        profileImage.image = posting.profileImage
        bodyTextView.text = posting.body
    }
}

