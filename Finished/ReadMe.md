    //Step 00 - Connect IBOutlet
    @IBOutlet var detailTextView: UITextView!




        //Step 01 - Test for loading data (fails by default)
        
        var wasCalled = false

        classUnderTest.loadUser(id: 1) { data in
            let result = String(decoding: data, as: UTF8.self)
            print("🍩 result: \(result)")
            wasCalled = true
        }
        
        //delay for network
        sleep(1)
        
        XCTAssertTrue(wasCalled)






        //Step 02 - Load Data from URL
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else { assertionFailure("😢🦄: Error‼️") ; return }

            success(data)
        }.resume()






//Step 03 - Create Stuct for JSON
//https://jsonplaceholder.typicode.com/users/1

struct User: Codable {
    var id: Int
    var name: String
    var username: String
    var email: String
    var address: Address
    var phone: String
    var website: String
    var company: Company
}

struct Company: Codable {
    var name: String
    var catchPhrase: String
    var bs: String
}

struct Address: Codable {
    var street: String
    var suite: String
    var city: String
    var zipcode: String
    var geo: Geo
}

struct Geo: Codable {
    var lat: String
    var lng: String
}







        //Step 04 - Decode JSON
        classUnderTest.loadUser(id: 1) { data in
            let user = try! JSONDecoder().decode(User.self, from: data)
            print("user: \(user)")
            XCTAssertEqual(user.address.geo.lat, "-37.3159")
        }
        
        sleep(1)





        //Step 05 - Unit Test - Pass Case
        let user = classUnderTest.toUser(data: testData)!
        XCTAssertEqual(user.address.geo.lat, "-37.3159")



        //Step 06 - Unit Test - Fail Case
        let badData = "not JSON".data(using: .utf8)!
        let user = classUnderTest.toUser(data: badData)
        XCTAssertNil(user)



        //Step 07 - Configure the cell
        cell.username.text = "🦄Test"




        //Step 08 - Present View

        let viewController = UIStoryboard.main.instantiateViewController(withIdentifier: DetailViewController.viewControllerId) as! DetailViewController
        
        //Step 10 - set text �

        //Step 12A - prepare view

        present(viewController, animated: true)
        // Step 09 --comment line above.  uncomment line below
        //navigationController?.pushViewController(viewController, animated: true)

        //End Step 08








        //Step 10 - set text � //comment out after crash
        viewController.detailTextView.text = "something"



        //Step 11 - set text 😱
        detailTextView.text = detailText





    //Step 12A - prepare view
    func prepare(with detailText: String) {
        self.detailText = detailText
    }





        //Step 12B - prepare view
        viewController.prepare(with: "� Do something magical �")







