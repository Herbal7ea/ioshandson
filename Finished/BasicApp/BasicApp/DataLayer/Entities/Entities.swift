//
//  Entities.swift
//  BasicApp
//
//  Created by herbal7ea on 4/16/19.
//  Copyright © 2019 Jon Bott. All rights reserved.
//

import Foundation

//Step 03 - Create Stuct for JSON
//https://jsonplaceholder.typicode.com/users/1

struct User: Codable {
    var id: Int
    var name: String
    var username: String
    var email: String
    var address: Address
    var phone: String
    var website: String
    var company: Company
}

struct Company: Codable {
    var name: String
    var catchPhrase: String
    var bs: String
}

struct Address: Codable {
    var street: String
    var suite: String
    var city: String
    var zipcode: String
    var geo: Geo
}

struct Geo: Codable {
    var lat: String
    var lng: String
}
