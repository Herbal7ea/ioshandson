//
//  NetworkLayer.swift
//  BasicApp
//
//  Created by herbal7ea on 4/10/19.
//  Copyright © 2019 Jon Bott. All rights reserved.
//

import Foundation

public typealias DataClosure = (Data) -> Void

class NetworkLayer {
    
    func loadUser(id: UInt, success: @escaping DataClosure) {
        let url = URL(string: "https://jsonplaceholder.typicode.com/users/\(id)")!

        //Step 02 - Load Data from URL
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else { assertionFailure("😢🦄: Error‼️") ; return }
            
            success(data)
        }.resume()
    }
}
