//
//  Translation.swift
//  BasicApp
//
//  Created by herbal7ea on 4/16/19.
//  Copyright © 2019 Jon Bott. All rights reserved.
//

import Foundation

class TranslationLayer {
    func toUser(data: Data) -> User? {
        let user = try? JSONDecoder().decode(User.self, from: data)
        return user
    }
}
