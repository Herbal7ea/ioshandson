//
//  TableViewController.swift
//  BasicApp
//
//  Created by herbal7ea on 4/10/19.
//  Copyright © 2019 Jon Bott. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 10
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewPostingCell", for: indexPath) as! NewPostingCell

        //Step 07 - Configure the cell
        cell.username.text = "🦄Test"


        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //Step 08 - Present View
        
        let viewController = UIStoryboard.main.instantiateViewController(withIdentifier: DetailViewController.viewControllerId) as! DetailViewController
        
        //Step 10 - set text 😱 //comment out after crash
        //viewController.detailTextView.text = "something"

        
        //Step 12B - prepare view
        viewController.prepare(with: "🦄 Do something magical 🌈")

        //present(viewController, animated: true)
        // Step 09 --comment line above.  uncomment line below
        navigationController?.pushViewController(viewController, animated: true)
        
        //End Step 08
    }
}

extension UIStoryboard {
    static var main: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }
}
