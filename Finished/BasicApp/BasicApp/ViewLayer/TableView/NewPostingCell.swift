//
//  NewPostingCell.swift
//  BasicApp
//
//  Created by herbal7ea on 4/10/19.
//  Copyright © 2019 Jon Bott. All rights reserved.
//

import UIKit

class NewPostingCell: UITableViewCell {

    static var cellId = "PostingCell"
    
    @IBOutlet var username: UILabel!
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var descriptionLabel: UILabel!
}
