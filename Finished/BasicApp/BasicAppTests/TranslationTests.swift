//
//  TranslationTests.swift
//  BasicAppTests
//
//  Created by herbal7ea on 4/16/19.
//  Copyright © 2019 Jon Bott. All rights reserved.
//

import XCTest
@testable import BasicApp

class TranslationTests: XCTestCase {
    
    private var classUnderTest = TranslationLayer()
    private var testData = """
{
    "id": 1,
    "name": "Leanne Graham",
    "username": "Bret",
    "email": "Sincere@april.biz",
    "address": {
        "street": "Kulas Light",
        "suite": "Apt. 556",
        "city": "Gwenborough",
        "zipcode": "92998-3874",
        "geo": {
            "lat": "-37.3159",
            "lng": "81.1496"
        }
    },
    "phone": "1-770-736-8031 x56442",
    "website": "hildegard.org",
    "company": {
        "name": "Romaguera-Crona",
        "catchPhrase": "Multi-layered client-server neural-net",
        "bs": "harness real-time e-markets"
    }
}
""".data(using: .utf8)!
    
    // MARK: - Unit Test
    func test_toUser_ParsesCorrectly() {
        //Step 05 - Unit Test - Pass Case
        let user = classUnderTest.toUser(data: testData)!
        XCTAssertEqual(user.address.geo.lat, "-37.3159")
    }
    
    func test_toUser_isNilIfBadData() {
        //Step 06 - Unit Test - Fail Case
        let badData = "not JSON".data(using: .utf8)!
        let user = classUnderTest.toUser(data: badData)
        XCTAssertNil(user)
    }
}
