//
//  BasicAppTests.swift
//  BasicAppTests
//
//  Created by herbal7ea on 4/10/19.
//  Copyright © 2019 Jon Bott. All rights reserved.
//

import XCTest
@testable import BasicApp

class NetworkTests: XCTestCase {

    private var classUnderTest = NetworkLayer()
    
    // MARK: - Integration Test
    func test_loadUser_successBlockCalled() {
        //Step 01 - Test for loading data (fails by default)
        
        var wasCalled = false
        
        classUnderTest.loadUser(id: 1) { data in
            let result = String(decoding: data, as: UTF8.self)
            print("🍩 result: \(result)")
            wasCalled = true
        }
        
        //delay for network
        sleep(1)
        
        XCTAssertTrue(wasCalled)
    }
    
    // MARK: - Unit Test
    func test_loadUser_ParseJSON() {
        //Step 04 - Decode JSON
        classUnderTest.loadUser(id: 1) { data in
            let user = try! JSONDecoder().decode(User.self, from: data)
            print("user: \(user)")
            XCTAssertEqual(user.address.geo.lat, "-37.3159")
        }
        
        sleep(1)

    }
}




